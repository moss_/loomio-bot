PYTHON ?= python3
PYTHON_FILES = *.py

test: isort-test black-test flake8-test pylint-test

fix: isort black

black:
	black $(PYTHON_FILES)

isort:
	isort $(PYTHON_FILES)

black-test:
	black --check $(PYTHON_FILES)

flake8-test:
	flake8 $(PYTHON_FILES)

pylint-test:
	flake8 $(PYTHON_FILES)

isort-test:
	isort --check-only $(PYTHON_FILES)
