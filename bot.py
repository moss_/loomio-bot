# Copyright 2020 Felipe Morato me@fmorato.com
#
# This file is under the MIT License.
# See the file `LICENSE` for details.
import asyncio
import logging
import os
import re
import sys
import urllib.parse
from base64 import b64decode
from io import BytesIO
from typing import List, Optional

import ujson
from aiohttp import ClientConnectorError, ClientSession
from pydantic import BaseModel, ValidationError
from sanic import Sanic
from sanic.log import LOGGING_CONFIG_DEFAULTS
from sanic.request import Request
from sanic.response import empty
from telethon import TelegramClient

logging.getLogger("telethon").setLevel(logging.WARNING)
logger = logging.getLogger("loomio-bot")

logging_config = LOGGING_CONFIG_DEFAULTS
logging_config["loggers"]["loomio-bot"] = {
    "level": "DEBUG",
    "handlers": ["loomio-bot"],
}
logging_config["handlers"]["loomio-bot"] = {
    "class": "logging.StreamHandler",
    "formatter": "loomio-bot",
    "stream": sys.stdout,
}
logging_config["formatters"]["loomio-bot"] = {
    "format": "%(asctime)s [%(process)d] [%(levelname)s] (%(name)s) "
    "[%(module)s::%(funcName)s():%(lineno)d] %(message)s",
    "datefmt": "[%Y-%m-%d %H:%M:%S]",
    "class": "logging.Formatter",
}
SESSION = os.environ.get("SESSION", "loomio-bot")
try:
    API_ID = int(os.environ.get("API_ID"))
    API_HASH = os.environ.get("API_HASH")
    API_TOKEN = os.environ.get("API_TOKEN")
    CHAT_ID = int(os.environ.get("CHAT_ID"))
except TypeError:
    logger.error("At least one of the env vars was not set")
    raise

DEBUG_CHAT_ID = int(os.environ.get("DEBUG_CHAT_ID", 0))

app = Sanic(__name__, log_config=logging_config)
loop = asyncio.get_event_loop()
image_queue = asyncio.Queue()
attachment_queue = asyncio.Queue()
bot = TelegramClient(SESSION, API_ID, API_HASH)

session: Optional[ClientSession] = None
re_mention_replace = re.compile(r"(\W)@")

# matches 1 group if it's a link and matchs 2 groups if base 64: 1. whole thing; 2. encoded image
re_image = re.compile(r"!\[\]\((data:image/\w{3,4};base64,(.*)|.*?)\)")
re_image_replace = re.compile(r"\n*!\[\]\((.*/(.*?))\)")
re_underline_replace = re.compile(r"</?u>")
re_italics_replace = re.compile(r"(\W)(_.*?_)(\W)")
re_heading_replace = re.compile(r"(^#+ )(.*$)", re.MULTILINE)

MAXIMUM_MESSAGE_LENGTH = 4096
BASE_URL = "https://www.loomio.org"


class Attachment(BaseModel):
    id: int
    icon: str
    filename: str
    byte_size: int
    signed_id: str
    preview_url: Optional[str]
    content_type: str
    download_url: str


class Event(BaseModel):
    # The webhook address this event was sent to
    token: Optional[str]
    # event content
    text: Optional[str]
    # icon of the grounp
    icon_url: Optional[str]
    # always "Loomio"
    username: Optional[str]
    # kind of event: new_comment, stance_created (for votes), poll_expired
    kind: Optional[str]
    # event_url
    url: Optional[str]
    group_name: Optional[str]
    # empty for automatic system actions, as poll closed
    actor_name: Optional[str]
    # set when event is a poll. Seen values: proposal
    poll_type: Optional[str]
    # Loomio Thread title
    title: Optional[str]
    attachments: Optional[List[Attachment]]


def split_large_message(message: str) -> []:
    queue = []
    while True:
        if len(message) > MAXIMUM_MESSAGE_LENGTH:
            pos = message[:MAXIMUM_MESSAGE_LENGTH].rfind("\n")
            if pos < 0:  # no new line found
                pos = message[:MAXIMUM_MESSAGE_LENGTH].rfind(" ")
            if pos < 0:
                pos = MAXIMUM_MESSAGE_LENGTH
            queue.append(message[:pos])
            message = message[pos + 1 :]
        else:
            queue.append(message)
            return queue[::-1] or [message]


async def worker(worker_name, worker_type, queue):
    while True:
        chat_id, msg_id, files = await queue.get()
        logger.debug("%s received job %s", worker_name, files)
        downloaded = []
        logger.debug("%s", files)
        for name, url_or_base64 in files.items():
            d = await get_file(url_or_base64, name)
            if not d:
                logger.error("Failed to download %r %s", name, url_or_base64)
                continue
            downloaded.append(d)
        logger.debug("%s downloaded %s", worker_name, files)

        if downloaded:
            try:
                if worker_type == "attachment":
                    for item in downloaded:
                        await bot.send_file(
                            chat_id, item, silent=True, reply_to=msg_id,
                        )
                else:
                    await bot.send_file(
                        chat_id,
                        downloaded,
                        caption=[name for name in files.keys()],
                        silent=True,
                        reply_to=msg_id,
                    )
            except Exception as e:
                logger.error(e, exc_info=True)

        logger.debug("%s completed job %s", worker_name, files)
        queue.task_done()


@app.listener("before_server_start")
async def startup(app, loop):
    logger.debug("starting bot")
    await bot.start(bot_token=API_TOKEN)


async def get_file(url_or_base64, filename: str = None) -> Optional[BytesIO]:
    global session
    if not session or session.closed:
        session = ClientSession(headers={"User-Agent": "loomio-bot/1.0"},)
    if not url_or_base64.startswith("http"):
        logger.debug(
            "file appears to be base64 encoded %s...", url_or_base64[:30],
        )
        return BytesIO(b64decode(url_or_base64))
    try:
        logger.debug(
            "getting %r %s", filename or "", url_or_base64,
        )
        async with session.get(url_or_base64) as response:
            logger.debug(
                "%s %r %s Bytes for %r %s",
                response.status,
                response.content_type,
                response.content_length,
                filename or "",
                response.url,
            )
            file = BytesIO(await response.read())
            if filename:
                file.name = urllib.parse.unquote(filename)
            return file
    except ClientConnectorError as e:
        logger.error(e, exc_info=True)
        return None


async def send(chat_id: int, message: str, **kwargs):
    logger.debug("Sending %r bytes long message: %s", len(message), message)
    message_queue = split_large_message(message)
    msg_id = None
    while message_queue:
        msg_id = await bot.send_message(chat_id, message=message_queue.pop(), **kwargs)
    return msg_id


async def send_debug(request: Request, error: str, message: str, force: bool = False):
    if not (error or force):
        return
    if DEBUG_CHAT_ID:
        raw_message = request.body
        parsed_message = ujson.dumps(
            request.json, ensure_ascii=False, escape_forward_slashes=False, indent=2
        )
        message = (
            f"RAW\n{raw_message}\n\nJSON\n{parsed_message}\n\nPROCESSED\n{message}"
        )
        if error:
            message = f"⚠FAILED⚠\n{error}\n\n{message}"

        await send(
            DEBUG_CHAT_ID, message=message, link_preview=False, parse_mode=None,
        )
    else:
        logger.error("DEBUG_CHAT_ID not set")


def make_link(name, url):
    url = url.replace("(", "%28").replace(")", "%29")
    return f"[{name}]({url})"


def image_name(match):
    if match.group(1).startswith("http"):
        return f"\n\n[{urllib.parse.unquote(match.group(2))}]({match.group(1)})\n\n"
    return f"\n\n[IMAGE]\n\n"


def heading_upper(match):
    return f"**{match.group(2).upper()}**"


@app.post("/")
async def loomio_webhook(request: Request):
    logger.debug("message received: %r", request.body)
    error = ""
    message = ""
    force = False
    try:
        event = Event(**request.json)
        message = event.text
        logger.debug("event: %r", event.dict())
        images = {
            urllib.parse.unquote(img[0][img[0].rfind("/") + 1 :])
            if img[0].startswith("http")
            else f"unamed image {index+1}": img[0]
            if img[0].startswith("http")
            else img[1]
            for index, img in enumerate(re_image.findall(message))
        }
        message = re_image_replace.sub(image_name, message)
        message = re_mention_replace.sub(r"\1", message)
        message = re_underline_replace.sub(r"**", message)
        message = re_italics_replace.sub(r"\1_\2_\3", message)
        message = re_heading_replace.sub(heading_upper, message)

        # insert [group_name] right before Thread title, inside the link
        pos = message.find("[") + 1
        message = f"{message[:pos]}[{event.group_name}] {message[pos:]}"
        logger.debug("processed message: %r", message)
        chat_id = CHAT_ID
        if event.group_name in {"TEST", "TESTING"}:
            chat_id = DEBUG_CHAT_ID or CHAT_ID
            force = True
        attachments = {}
        if event.attachments:
            attachments = {
                a.filename: f"{BASE_URL}{a.download_url}" for a in event.attachments
            }
            message += "\n\n**Attachments**"
            for filename, url in attachments.items():
                message += f"\n{make_link(filename, url)}"

        msg_id = await send(chat_id, message=message, link_preview=False)
        if images:
            image_queue.put_nowait((chat_id, msg_id, images))
        if attachments:
            attachment_queue.put_nowait((chat_id, msg_id, attachments,))
    except ValidationError as e:
        logger.error(e, exc_info=True)
        error = e.json()
    except Exception as e:
        logger.error(e, exc_info=True)
        error = str(e)

    await send_debug(request, error, message, force)
    return empty()


if __name__ == "__main__":
    server = app.create_server(
        host="0.0.0.0",
        port=os.environ.get("PORT", 8000),
        debug=os.environ.get("DEBUG", False),
        return_asyncio_server=True,
    )
    server_task = asyncio.ensure_future(server)
    for i in range(1, 3):
        asyncio.ensure_future(worker(f"worker-{i}-image", "image", image_queue))
        asyncio.ensure_future(
            worker(f"worker-{i}-attachment", "attachment", attachment_queue)
        )
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logger.info("Cleaning up")
        image_queue.join()
        attachment_queue.join()
        session.close()
        loop.stop()
        server.close()
