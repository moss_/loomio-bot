# from https://github.com/pmac/multi-stage-docker-venv-demo/blob/master/Dockerfile
FROM python:slim AS builder

WORKDIR /app
ENV LANG=C.UTF-8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PATH="/venv/bin:$PATH"

RUN python -m venv /venv
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt


FROM python:slim AS app

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PATH="/venv/bin:$PATH"
RUN adduser --uid 1000 --disabled-password --gecos '' --no-create-home webdev
ENV PORT=5000

WORKDIR /app
EXPOSE 5000
CMD uvicorn bot:app --host=0.0.0.0 --port=${PORT:=5000} --log-level=info

COPY --from=builder /venv /venv

COPY ./bot.py ./

RUN chown webdev.webdev -R .
USER webdev
