# Loomio Telegram bot
This bot receives messages from Loomio's webhooks and sends them to a `CHAT_ID`, which can be a conversation, group or channel.

## Intro
For a conversation, a conversation with the bot must have been started previously. For groups or channels, the bot must have been added beforehand.

### Setting up the bot
Write to `@BotFather` to create a bot and get the API_TOKEN.

`API_ID` and `API_HASH` can be obtained from https://my.telegram.org.

You need to deploy the bot once for each `CHAT_ID` who will receive the messages.

**NB**:
The bot does not reply to messages. There is no database or storage of any kind. Loomio events are forwarded and that's it. If there are any errors the event will be lost.

### Features
This bot receives the webhook json, does data validation on it and processes the message.

Processing done is:
- Inserts Loomio's `[group name]` in the thread title, so that messages from multiple groups going to the same `CHAT_ID` have more context
- Extract inline images, downloads and send them as a gallery
- Extracts attachments, downloads and sends them as documents
- Removes @ mentions as they may tag unwanted Telegram user/channel/group
- Replaces underline emphasis (`<u>underlined here</u>`) with bold (`**bold here**`), as Telegram clients don't parse the former
- Translates Loomio italics to Telegram italics. From `_loomio italics_` to `__text in italics__`
- Sends debugging messages to `DEBUG_CHAT_ID`
- Splits large messages to respect Telegram's limit of 4096 chars

All the components are async, so even if it is downloading larger files and images it should still be able to process other incoming events.

## Debugging
Debugging messages are sent to `DEBUG_CHAT_ID` if it is set. These messages are only sent if there was an error.

For testing, you can setup a private Loomio group called TEST or TESTING and any event from that group will be sent only to `DEBUG_CHAT_ID`, including processed message and debugging data.

If you send a message from a groupd called TEST or TESTING and there is no `DEBUG_CHAT_ID` set, the debugging info will go `CHAT_ID`.

    python bot.py
    
### Testing the endpoint

    import requests
    data = {
        "token": "https://loomio-bot.herokuapp.com/",
        "text": "Message here",
        "icon_url": "https://loomio-uploads.s3.amazonaws.com/groups/logos/000/651/553/medium/profile_alternative_blue.png?is_here",
        "username":"Loomio", 
        "kind":"stance_created",
        "url":"https://www.loomio.org/p/hash_here",
        "group_name": "GROUP",
        "actor_name": "Person name or empty",
        "poll_type": "proposal",
        "title": "In a thread, show new comments first!"
    }
    r = requests.post("http://0.0.0.0:8000", json=data)
    assert r.status_code, 204

### Finding chat ids
Use ipython so there's no need to call asyncio.

    %autoawait
    from telethon import TelegramClient, utils
    API_ID = 11111
    API_HASH = "hash_here"
    API_TOKEN = "numbers:token"
    client = TelegramClient("loomio-bot", API_ID, API_HASH).start(bot_token=API_TOKEN)
    
    # using invite link for groups and channels
    invite_link = "t.me/joinchat/code_here"
    chat_id = await client.get_peer_id(invite_link)
    
    # using username, phonenumber in international format
    username = "@someone"
    username = "+4912393145"
    username = "t.me/someone"
    chat_id = await client.get_entity(username)
    
## Production

    uvicorn bot:app --host=0.0.0.0 --port=8000 --log-level=info
    
## Dependencies
The main dependencies are:
- [sanic: webserver](https://sanic.readthedocs.io/en/latest/)
- [telethon: telegram library](https://docs.telethon.dev/en/latest/)
- [uvicorn: ASGI server](https://www.uvicorn.org/)
- [pydantic: data validation](https://pydantic-docs.helpmanual.io/)
- [ujson: parsing json message](https://github.com/ultrajson/ultrajson) (regular json package fails)
- [aiohttp: download images and attachments](https://docs.aiohttp.org/en/stable/)

Install with either

    pip install -Ur requirements.txt
    
or 

    poetry install
